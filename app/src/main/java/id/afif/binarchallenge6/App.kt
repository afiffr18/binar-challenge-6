package id.afif.binarchallenge6

import android.app.Application
import id.afif.binarchallenge6.helper.DataStoreManager
import id.afif.binarchallenge6.helper.UserRepo
import id.afif.binarchallenge6.viewmodel.MoviesViewModel
import id.afif.binarchallenge6.viewmodel.UserViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.GlobalContext.startKoin
import org.koin.dsl.module

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        initKoin()
    }

    private fun initKoin() {
        startKoin {
            androidContext(this@App)
            modules(listOf(appModule))
        }
    }

    private val appModule = module {
        single { UserRepo(get()) }
        single { MoviesViewModel(get()) }
        single { DataStoreManager(get()) }
        single { UserViewModel(get()) }
    }
}